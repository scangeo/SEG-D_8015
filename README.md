# SEG-D 8015

Conversion from <em>2<sup>½</sup> byte (20 bit) binary exponent data recording method demultiplexed</em> to <a href="https://en.wikipedia.org/wiki/IEEE floating point" class="interwiki iw_wp" title="https://en.wikipedia.org/wiki/IEEE floating point">IEEE floating point</a> single-precision format.

## Description from the Society of Exploration Geophysicists (SEG)

<table>
  <thead>
    <tr>
      <th>Bit</th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <td></td>
    </tr>
  </thead>
  <tr>
    <th>Byte 1 </th>
    <td> C<sub>3</sub> </td>
    <td> C<sub>2</sub> </td>
    <td> C<sub>1</sub> </td>
    <td> C<sub>0</sub> </td>
    <td> C<sub>3</sub> </td>
    <td> C<sub>2</sub> </td>
    <td> C<sub>1</sub> </td>
    <td> C<sub>0</sub> </td>
    <td> Exponents 1-2 </td>
  </tr>
  <tr>
    <th>Byte 2 </th>
    <td> C<sub>3</sub> </td>
    <td> C<sub>2</sub> </td>
    <td> C<sub>1</sub> </td>
    <td> C<sub>0</sub> </td>
    <td> C<sub>3</sub> </td>
    <td> C<sub>2</sub> </td>
    <td> C<sub>1</sub> </td>
    <td> C<sub>0</sub> </td>
    <td> Exponents 3-4</td>
  </tr>
  <tr>
    <th>Byte 3 </th>
    <td> S </td>
    <td> Q<sub>-1</sub> </td>
    <td> Q<sub>-2</sub> </td>
    <td> Q<sub>-3</sub> </td>
    <td> Q<sub>-4</sub> </td>
    <td> Q<sub>-5</sub> </td>
    <td> Q<sub>-6</sub> </td>
    <td> Q<sub>-7</sub> </td>
    <td rowspan="2"> Sample 1 </td>
  </tr>
  <tr>
    <th>Byte 4 </th>
    <td> Q<sub>-8</sub> </td>
    <td> Q<sub>-9</sub> </td>
    <td> Q<sub>-10</sub> </td>
    <td> Q<sub>-11</sub> </td>
    <td> Q<sub>-12</sub> </td>
    <td> Q<sub>-13</sub> </td>
    <td> Q<sub>-14</sub> </td>
    <td> Q<sub>-15</sub> </td>
  </tr>
  <tr>
    <th>Byte 5 </th>
    <td> S </td>
    <td> Q<sub>-1</sub> </td>
    <td> Q<sub>-2</sub> </td>
    <td> Q<sub>-3</sub> </td>
    <td> Q<sub>-4</sub> </td>
    <td> Q<sub>-5</sub> </td>
    <td> Q<sub>-6</sub> </td>
    <td> Q<sub>-7</sub> </td>
    <td rowspan="2"> Sample 2 </td>
  </tr>
  <tr>
    <th>Byte 6 </th>
    <td> Q<sub>-8</sub> </td>
    <td> Q<sub>-9</sub> </td>
    <td> Q<sub>-10</sub> </td>
    <td> Q<sub>-11</sub> </td>
    <td> Q<sub>-12</sub> </td>
    <td> Q<sub>-13</sub> </td>
    <td> Q<sub>-14</sub> </td>
    <td> Q<sub>-15</sub> </td>
  </tr>
  <tr>
    <th>Byte 7 </th>
    <td> S </td>
    <td> Q<sub>-1</sub> </td>
    <td> Q<sub>-2</sub> </td>
    <td> Q<sub>-3</sub> </td>
    <td> Q<sub>-4</sub> </td>
    <td> Q<sub>-5</sub> </td>
    <td> Q<sub>-6</sub> </td>
    <td> Q<sub>-7</sub> </td>
    <td rowspan="2"> Sample 3 </td>
  </tr>
  <tr>
    <th>Byte 8 </th>
    <td> Q<sub>-8</sub> </td>
    <td> Q<sub>-9</sub> </td>
    <td> Q<sub>-10</sub> </td>
    <td> Q<sub>-11</sub> </td>
    <td> Q<sub>-12</sub> </td>
    <td> Q<sub>-13</sub> </td>
    <td> Q<sub>-14</sub> </td>
    <td> Q<sub>-15</sub> </td>
  </tr>
  <tr>
    <th>Byte 9 </th>
    <td> S </td>
    <td> Q<sub>-1</sub> </td>
    <td> Q<sub>-2</sub> </td>
    <td> Q<sub>-3</sub> </td>
    <td> Q<sub>-4</sub> </td>
    <td> Q<sub>-5</sub> </td>
    <td> Q<sub>-6</sub> </td>
    <td> Q<sub>-7</sub> </td>
    <td rowspan="2"> Sample 4 </td>
  </tr>
  <tr>
    <th>Byte 10 </th>
    <td> Q<sub>-8</sub> </td>
    <td> Q<sub>-9</sub> </td>
    <td> Q<sub>-10</sub> </td>
    <td> Q<sub>-11</sub> </td>
    <td> Q<sub>-12</sub> </td>
    <td> Q<sub>-13</sub> </td>
    <td> Q<sub>-14</sub> </td>
    <td> Q<sub>-15</sub> </td>
  </tr>
</table>


S = sign bit — Binary one means negative number.

C = binary exponents — This is a 4 bit positive binary exponent of 2 written as 2<sup>CCCC</sup> where CCCC can assume values of 0-15. The four exponents are in channel number order for the four channels starting with channel one in bits 0-3 of Byte 1.

Q<sub>-1-15</sub> = fraction — This is a 15 bit <a href="https://en.wikipedia.org/wiki/ones' complement" class="interwiki iw_wp" title="https://en.wikipedia.org/wiki/ones' complement">ones&#039; complement</a> binary fraction. The radix point is to the left of the most significant bit (Q<sub>-1</sub>) with the MSB being defined as 2<sup>-1</sup>. The sign and fraction can assume values from 1 - 2<sup>-15</sup> to -1 + 2<sup>-15</sup>.

Note that bit 7 of the second byte of each sample must be zero in order to guarantee the uniqueness of the
start of scan. Negative zero is invalid and must be convened to positive zero.

Input signal = S.QQQQ,QQQQ,QQQQ,QQQ * 2<sup>CCCC</sup> * 2<sup>MP</sup> millivolts where 2<sup>MP</sup> is the value required to descale the data word to the recording system input level. MP is defined in Byte 8 of each of the corresponding channel set descriptors in the scan type header.

Note that in utilizing this data recording method, the number of data channels per channel set must be exactly divisible by 4 in order to preserve the data grouping of this method.

Source: [Barry, K. M., Cavers, D. A. and Kneale, C. W., 1975, Report on recommended standards for digital tape formats: Geophysics, 40, no. 02, 344-352.](http://seg.org/Portals/0/SEG/News%20and%20Resources/Technical%20Standards/seg_d_rev0.pdf)

## Ruby

Converts a 10 bytes block of 4 samples of FILENAME at OFFSET and displays the result. This ruby code is deliberately complicated to be as similar as possible to the C version.

    ./sample_8015_to_Float.rb FILENAME OFFSET

## C

Converts a 10 bytes block of 4 samples of FILENAME at OFFSET and displays the result.

Compile with the below Makefile & run:

    make sample_8015_to_Float
    ./sample_8015_to_Float FILENAME OFFSET
