#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h> 
#include <math.h>

int main(int argc, char *argv[])
{
  char *filename = argv[1]; // C3590q-1.raw
  int offset = atoi(argv[2]); // 9940

  struct {
    uint8_t exponents[2];
    int16_t fractions[4];
  } input;
  
  struct {
    float samples[4];
  } output;
  
  uint8_t exponents[4];
  int i;
  int16_t fraction;


  printf("Filename: %s\n", filename);
  printf("Offset: %d\n", offset);

  // Read file
  FILE *fp;
  fp = fopen(filename, "r");
  fseek(fp, offset, SEEK_SET);
  fread(&input, sizeof(input), 1, fp);
  
  // Extract the 4 4-bit exponents from the 2 first bytes.
  exponents[0] = input.exponents[0] >> 4;
  exponents[1] = input.exponents[0] & 15;
  exponents[2] = input.exponents[1] >> 4;
  exponents[3] = input.exponents[1] & 15;

  // Calculate the 4 samples values'.
  for (i = 0; i < 4; i++)
  {
    // Convert from big-endian to little-endian.
    fraction = __builtin_bswap16(input.fractions[i]);
    
    // Convert from two's complement to one's complement if negative.
    if (fraction < 0) fraction = -(~fraction);
    
    output.samples[i] = ldexpf(fraction, exponents[i] - 15);
    
    printf("Sample %d/4: %8d / 2**15 * 2**(%2d) = %f\n", 
      i+1, fraction, exponents[i], output.samples[i]);
  }
  
  fclose(fp);

  return 0;
}

