#!/usr/bin/env ruby

begin
  filename = ARGV[0]
  offset = ARGV[1].to_i

  class Input
    def initialize
      @exponents = []
      @fractions = []
    end
    attr_accessor :exponents, :fractions
  end
  input = Input.new

  class Output
    def initialize
      @samples = []
    end
    attr_accessor :samples
  end
  output = Output.new

  exponents = []

  printf("Filename: %s\n", filename);
  printf("Offset: %d\n", offset);

  # Read file
  fp = File.open(filename, "r");
  fp.seek(offset, IO::SEEK_SET);
  unpacked = fp.read(10).unpack("C2s>4")
  input.exponents = unpacked[0, 2]
  input.fractions = unpacked[2, 4]

  # Extract the 4 4-bit exponents from the 2 first bytes.
  exponents[0] = input.exponents[0] >> 4;
  exponents[1] = input.exponents[0] & 15;
  exponents[2] = input.exponents[1] >> 4;
  exponents[3] = input.exponents[1] & 15;

  # Calculate the 4 samples values'.
  for i in 0..3 do
    fraction = input.fractions[i]

    # Convert from two's complement to one's complement if negative.
    fraction = -(~fraction) if (fraction < 0)

    output.samples[i] = fraction * 2 ** (exponents[i] - 15)
    printf("Sample %d/4: %8d / 2**15 * 2**(%2d) = %f\n",
      i+1, fraction, exponents[i], output.samples[i])
  end

  fp.close;
end
